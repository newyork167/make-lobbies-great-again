package mlga.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.URL;
import java.util.Map;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import mlga.geolocation.Geolocation;
import mlga.io.peer.IOPeer;
import mlga.io.peer.IOPeer.Status;

/** Simple wrapper to visually represent a Peer that is connected. */
public class Peer {
	private Inet4Address id;
	private long ping = 0;
	private boolean blocked;
	private boolean hasStatus = false;
	private long last_seen;
	private String hostname;
	private String geolocation_country;
	private String geolocation_city;
	private String geolocation_state;

	public IOPeer io;

	public Peer(Inet4Address hash, long rtt, IOPeer io) {
		this.io = io;
		this.id = hash;
		this.ping = rtt;
		this.hostname = hash.getHostName();
		this.hasStatus = io.getStatus() != Status.UNRATED;
		if (this.hasStatus) {
			this.blocked = io.getStatus() == Status.BLOCKED;
		}
		this.last_seen = System.currentTimeMillis();

		try {
			Map<String, String> geoLocationData = Geolocation.GetGeoDataByIP(this.id);
			this.geolocation_country = geoLocationData.get(Geolocation.COUNTRY);

			if (geoLocationData.containsKey(Geolocation.CITY)) {
				this.geolocation_city = geoLocationData.get(Geolocation.CITY);
			}

			if (geoLocationData.containsKey(Geolocation.STATE)) {
				this.geolocation_state = geoLocationData.get(Geolocation.STATE);
			}

		} catch (IOException | GeoIp2Exception e) {
			e.printStackTrace();
		}
	}

	void setPing(long ping) {
		this.ping = ping;
		this.last_seen = System.currentTimeMillis();
	}

	Inet4Address getID() {
		return this.id;
	}

	/** Returns this Peer's ping. */
    long getPing() {
		return this.ping;
	}

	/** If we have saved, and also blocked, this Peer. */
    boolean blocked() {
		return this.hasStatus && this.blocked;
	}

	boolean camper() {
    	return this.io.getStatus() == Status.CAMPER;
	}

	/** Save our opinion of this Peer. */
    void rate(boolean block) {
		this.hasStatus = true;
		this.blocked = block;
		if (this.io.getStatus() == Status.BLOCKED && block){
			this.io.setStatus(Status.CAMPER);
		} else {
			this.io.setStatus(block ? Status.BLOCKED : Status.LOVED);
		}
//		this.io.setStatus(block ? Status.BLOCKED : Status.LOVED);
	}

	/** Remove this peer from the Preferences. */
    void unsave() {
		this.hasStatus = false;
		this.io.setStatus(Status.UNRATED);
	}

	/** If we've saved this Peer before. */
    boolean saved() {
		return this.hasStatus;
	}

	/** Returns the time (in milliseconds) since this Peer was last pinged. */
    long age() {
		return System.currentTimeMillis() - this.last_seen;
	}

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

	public String getGeolocation_country() {
		return geolocation_country;
	}

	public void setGeolocation_country(String geolocation_country) {
		this.geolocation_country = geolocation_country;
	}

	public String getGeolocation_city() {
		return geolocation_city;
	}

	public String getGeolocation_state() {
		return geolocation_state;
	}
}