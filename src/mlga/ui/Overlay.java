package mlga.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import mlga.Boot;
import mlga.io.FileUtil;
import mlga.io.Settings;
import mlga.io.peer.PeerTracker;

public class Overlay extends JPanel {
	private static final long serialVersionUID = -470849574354121503L;

	private boolean frameMove = false;

	private CopyOnWriteArrayList<Peer> peers = new CopyOnWriteArrayList<Peer>();
	private Font roboto;
	/** idx & fh are updated by listener and rendering events. <br>They track hovered index and font height. */
	private int idx = -1, fh = 0;

	private final PeerTracker peerTracker;

	private final JWindow frame;

	public Overlay() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, FontFormatException, IOException {
		peerTracker = new PeerTracker();
		peerTracker.start();

		InputStream is = FileUtil.localResource("Roboto-Medium.ttf");
		roboto = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(15f);
		is.close();

		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		this.setOpaque(false);
		frame = new JWindow();
		frame.setBackground(new Color(0, 0, 0, 0));
		frame.setFocusableWindowState(false);

		frame.add(this);
		frame.setAlwaysOnTop(true);
		frame.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!SwingUtilities.isRightMouseButton(e)) {
					if (e.isShiftDown()) {
						if (idx < 0 || idx >= peers.size() || peers.isEmpty() || e.getX() < 0 || e.getY() < 0)
							return;

						Peer p = peers.get(idx);
						if (!p.saved()) {
							p.rate(true);
						} else if (p.blocked()) {
							if (!p.camper()){
								p.rate(true);
							} else {
								p.rate(false);
							}
						} else {
							p.unsave();
						}
					} else if (e.getClickCount() >= 2) {
						frameMove = !frameMove;
						Settings.set("frame_x", frame.getLocationOnScreen().x);
						Settings.set("frame_y", frame.getLocationOnScreen().y);
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
				idx = -1;
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}
		});
		frame.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (frameMove)
					frame.setLocation(e.getXOnScreen() - (getPreferredSize().width / 2), e.getYOnScreen() - 6);
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				idx = Math.min(peers.size() - 1, (int) Math.floor(e.getY() / (fh)));
			}

		});

		frame.pack();
		frame.setLocation((int) Settings.getDouble("frame_x", 5), (int) Settings.getDouble("frame_y", 400));
		frame.setVisible(true);

		Timer cleanTime = new Timer();
		cleanTime.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				peers.stream().filter(p -> p.age() >= 5000).forEach(p -> {
					Boot.active.remove(p.getID().hashCode());
					peers.remove(p);
				});
			}
		}, 0, 2500);

		Thread t = new Thread("UIPainter") {
			public void run() {
				try {
					while (true) {
						frame.toFront(); //Fix for window sometime hiding behind others
						if (!frameMove) {
							Thread.sleep(400);
						} else {
							Thread.sleep(10);
						}
						Overlay.this.repaint();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.setDaemon(true);
		t.start();
	}

	private void addPeer(Inet4Address addr, long rtt) {
		peers.add(new Peer(addr, rtt, peerTracker.getPeer(addr)));
	}

	/** Sets a peer's ping, or creates their object. */
	public void setPing(Inet4Address id, long ping) {
		Peer p = this.getPeer(id);
		if (p != null) {
			p.setPing(ping);
		} else {
			this.addPeer(id, ping);
		}
	}

	/** Finds a Peer connection by its ID. */
	private Peer getPeer(Inet4Address id) {
		return peers.stream().filter(p -> p.getID().equals(id)).findFirst().orElse(null);
	}

	/** Dispose this Overlay's Window. */
	public void close() {
		this.frame.dispose();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(910, 200);
	}

	private class RttObject {
	    public String displayName;
	    public Color displayColor;
    }

    public static <T> T last(T[] array) {
        return array[array.length - 1];
    }

	@Override
	protected void paintComponent(Graphics gr) {
		super.paintComponent(gr);
		Graphics2D g = (Graphics2D) gr.create();
		g.setColor(getBackground());
		g.setFont(roboto);
		g.setColor(new Color(0, 0, 0, 0));
		g.fillRect(0, 0, getWidth(), getHeight());

		if (!frameMove) {
			g.setColor(new Color(0f, 0f, 0f, .5f));
		} else {
			g.setColor(new Color(0f, 0f, 0f, 1f));
		}

		fh = g.getFontMetrics().getAscent();//line height. Can use getHeight() for more padding between.

		String longestString = "";
        ArrayList<RttObject> renderArray = new ArrayList<>();
		if (!peers.isEmpty()) {
			short i = 0;
			for (Peer p : peers) {
				String calendar_time;
			    RttObject rttObject = new RttObject();
				if (idx == i) {
					g.setColor(new Color(0f, 0f, 0f));
					g.fillRect(1, fh * i + 1, getPreferredSize().width, fh + 1);//Pronounce hovered Peer.
				}
				long rtt = p.getPing();
				if (rtt <= 140) {
				    rttObject.displayColor = Color.GREEN;
				} else if (rtt > 140 && rtt <= 190) {
                    rttObject.displayColor = Color.YELLOW;
				} else {
                    rttObject.displayColor = Color.RED;
				}

				String geolocation_city = "N/A", geolocation_state = "N/A", geolocation_country = "N/A";

				if (p.getGeolocation_city() != null){
					geolocation_city = !p.getGeolocation_city().equals("") ? p.getGeolocation_city() : "N/A";
				}

				if (p.getGeolocation_state() != null) {
					geolocation_state = !p.getGeolocation_state().equals("") ? p.getGeolocation_state() : "N/A";
				}

				if (p.getGeolocation_country() != null) {
					geolocation_country = !p.getGeolocation_country().equals("") ? p.getGeolocation_country() : "N/A";
				}

				Calendar last_seen_calendar = Calendar.getInstance();
				last_seen_calendar.setTimeInMillis(p.io.getFirstSeen());

                String render = String.format("%s%s (%s) - (%s, %s, %s) - Ping: %d",
                        p.saved() ? (p.blocked() ?  p.camper() ? "CAMPER: " : "BLOCKED: " : "LOVED: " ) : "",
                        last(p.getID().toString().split("/")),
                        !p.getHostname().equals("") ? p.getHostname() : "N/A",
						geolocation_city,
						geolocation_state,
						geolocation_country,
                        rtt
                );

                try{
					Date date = last_seen_calendar.getTime();
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
					calendar_time = String.format("Time First Seen: %s", format1.format(date));

					if (calendar_time == null) {
						calendar_time = "N/A";
					}
				}
				catch (Exception ex) {
					calendar_time = "N/A";
				}

				if (render.length() > longestString.length()) {
				    longestString = render;
                }

                rttObject.displayName = render;
                renderArray.add(rttObject);

				RttObject lastSeenObject = new RttObject();
				lastSeenObject.displayName = calendar_time;
				lastSeenObject.displayColor = Color.GREEN;
                renderArray.add(lastSeenObject);

				++i;
			}
		} else {
		    RttObject rttObject = new RttObject();
		    rttObject.displayName = "No Players";
		    rttObject.displayColor = Color.RED;
			longestString = rttObject.displayName;

            renderArray.add(rttObject);
        }

        int rightPadding = 0;
        g.fillRect(0, 0, longestString.equals("") ? getPreferredSize().width : g.getFontMetrics().stringWidth(longestString) + rightPadding, fh * Math.max(1, peers.size()) + 2);
		g.fillRect(0, 0, 4000 + rightPadding, fh * 2 * Math.max(1, peers.size()) + 2);

        for (int i = 0; i < renderArray.size(); i++) {
            RttObject rttObject = renderArray.get(i);

            g.setColor(rttObject.displayColor);
            g.drawString(rttObject.displayName, 1, fh * (i + 1));
        }

		g.dispose();
	}
}
