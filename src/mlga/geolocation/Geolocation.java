package mlga.geolocation;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.CountryResponse;

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class Geolocation
{
    public static String COUNTRY = "country";
    public static String CITY = "city";
    public static String STATE = "state";

    public static Map<String, String> GetGeoDataByIP(Inet4Address ip) throws IOException, GeoIp2Exception {
        Map<String, String> geoData = new HashMap<>();
        String countryName;
        String cityName;
        String state;

        // Tries city first
        String dbLocation = "GeoLite2-City.mmdb";

        File database = new File(dbLocation);
        DatabaseReader dbReader = new DatabaseReader.Builder(database).build();

        CityResponse response = dbReader.city(ip);

        if (response != null) {
            countryName = response.getCountry().getName();
            cityName = response.getCity().getName();
            state = response.getLeastSpecificSubdivision().getName();

            geoData.put(CITY, cityName);
            geoData.put(STATE, state);
        }
        else {
            // Tries country last
            dbLocation = "GeoLite2-Country.mmdb";
            database = new File(dbLocation);
            dbReader = new DatabaseReader.Builder(database).build();

            CountryResponse countryResponse = dbReader.country(ip);

            countryName = countryResponse.getCountry().getName();
        }

        geoData.put(COUNTRY, countryName);

        return geoData;
    }
}